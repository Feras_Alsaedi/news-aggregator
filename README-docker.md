# React-Vite task project Dockerized Application

This project is a React application built with Vite, dockerized for easy deployment.

## Prerequisites

- Docker installed on your machine.

## Building the Docker Image

1. Navigate to the project directory.
2. Run the following command to build the Docker image:
   - docker build . -t task-front:latest
   - docker run -p 3000:3000 task-front:latest
