import { Button, Divider, Image, Table, Tooltip } from "antd";
import styles from "./style.module.css";
import { ColumnProps } from "antd/es/table";
import { LinkOutlined } from "@ant-design/icons";

interface IProps {
  tableData: any;
  title: string;
}
const NewsTable = ({ tableData, title }: IProps) => {
  const columns: ColumnProps<any>[] = [
    {
      title: "",
      dataIndex: "image",
      key: "image",
      render: (_, record) => {
        return (
          <Image
            alt="image article"
            src={record?.urlToImage ?? "/not-found.jpg"}
          />
        );
      },
    },
    {
      title: "title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Author",
      dataIndex: "author",
      key: "author",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },

    {
      title: "Actions",
      dataIndex: "",
      width: 200,
      align: "center",
      key: "x",
      render: (_: any, record) => (
        <Tooltip title={"Link"}>
          <Button
            type="primary"
            shape="circle"
            icon={<LinkOutlined />}
            onClick={() => window.open(record?.url, "_blank")}
          />
        </Tooltip>
      ),
    },
  ];
  return (
    <div className={styles.tableContainer}>
      <h1>{title}</h1>
      <Divider />
      <Table
        scroll={{
          x: "200",
        }}
        pagination={{ pageSize: 10, position: ["bottomCenter"] }}
        dataSource={tableData ?? []}
        columns={columns}
        // pagination={false}
      />
    </div>
  );
};

export default NewsTable;
