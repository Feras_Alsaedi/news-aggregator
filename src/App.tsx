import { useState } from "react";
import { useQuery } from "react-query";
import { Flex } from "antd";
import NewsService from "./services/news";
import "./App.css";
import Navbar from "./components/Navbar/Navbar";
import NewsForm from "./components/NewsForm/NewsForm";
import NyTable from "./components/NyTable/NyTable";
import GuardianTable from "./components/GuardianTable/GuardianTable";
import NewsTable from "./components/NewsApiTable/NewsTable";

function App() {
  const [searchTerm, setSearchTerm] = useState("");
  const [category] = useState<string | undefined>("");

  const [selectedDate, setSelectedDate] = useState<string | undefined>(
    undefined
  );
  const [sources, setSources] = useState<string[]>();

  const newApi = new NewsService();

  const newsApiQuery = useQuery<any>(
    ["newsApi", searchTerm, category, selectedDate],
    () => newApi.fetchNewsAPI(searchTerm, category, selectedDate),
    { enabled: !!searchTerm } // Only fetch when searchTerm has a value
  );

  const guardianQuery = useQuery<any>(
    ["guardian", searchTerm],
    () => newApi.fetchGuardian(searchTerm),
    { enabled: !!searchTerm } // Only fetch when searchTerm has a value
  );

  const nytQuery = useQuery<any>(
    ["nyt", searchTerm, selectedDate],
    () => newApi.fetchNYTimes(searchTerm, selectedDate),
    { enabled: !!searchTerm } // Only fetch when searchTerm has a value
  );

  const isLoading =
    newsApiQuery.isLoading || nytQuery.isLoading || guardianQuery.isLoading;
  const error: any =
    newsApiQuery.error || nytQuery.error || guardianQuery.error;

  // Separate articles by source
  const newsApiArticles = newsApiQuery.data || [];
  const guardianArticles = guardianQuery.data || [];
  const nytArticles = nytQuery.data || [];
  return (
    <>
      <Navbar />
      <div className="App">
        <Flex justify="center">
          <NewsForm
            loading={isLoading}
            submitData={(data: any) => {
              setSearchTerm(data?.search);
              setSelectedDate(data?.date);
              setSources(data?.sources ?? []);
            }}
          />
        </Flex>
        {error && <p>Error fetching articles: {error.message}</p>}

        {/* News */}
        {sources?.includes("news") && (
          <Flex justify="center">
            <NewsTable tableData={newsApiArticles} title="News API Articles" />
          </Flex>
        )}

        {/* Guardian */}
        {sources?.includes("guardian") && (
          <Flex justify="center">
            <GuardianTable
              tableData={guardianArticles}
              title="Guardian API Articles"
            />
          </Flex>
        )}

        {/* NY */}
        {sources?.includes("ny") && (
          <Flex justify="center">
            <NyTable tableData={nytArticles} title="NY API Articles" />
          </Flex>
        )}
      </div>
    </>
  );
}

export default App;
