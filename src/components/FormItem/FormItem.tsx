import React from "react";
import styles from "./style.module.css";

interface IProps {
  label?: string;
  isOptional?: boolean;
  required?: boolean;
  subTitle?: string;
  children: React.ReactNode;
}

const FormItem: React.FC<IProps> = ({
  label,
  required,
  isOptional,
  children,
  subTitle,
}) => {
  return (
    <div className={styles.formItem}>
      <label className={styles.formItemLabel}>
        {required && <span className={styles.star}>*</span>}
        {label} {isOptional && <span>Optional</span>}
        <span style={{ color: "lightgray", fontSize: "0.9rem" }}>
          {subTitle && subTitle}
        </span>
      </label>
      {children}
    </div>
  );
};

export default FormItem;
