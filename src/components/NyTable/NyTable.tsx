import { Button, Divider, Table, Tooltip } from "antd";
import styles from "./style.module.css";
import { ColumnProps } from "antd/es/table";
import { LinkOutlined } from "@ant-design/icons";

interface IProps {
  tableData: any;
  title: string;
}
const NyTable = ({ tableData, title }: IProps) => {
  const columns: ColumnProps<any>[] = [
    {
      title: "title",
      dataIndex: "abstract",
      key: "name",
    },
    {
      title: "Author",
      dataIndex: "author",
      key: "author",
      render: (_, record) => {
        return <>{record?.byline?.original ?? "-"}</>;
      },
    },
    {
      title: "Section",
      dataIndex: "section_name",
      key: "section_name",
    },

    {
      title: "Actions",
      dataIndex: "",
      width: 200,
      align: "center",
      key: "x",
      render: (_: any, record) => (
        <Tooltip title={"Link"}>
          <Button
            type="primary"
            shape="circle"
            icon={<LinkOutlined />}
            onClick={() => window.open(record?.web_url, "_blank")}
          />
        </Tooltip>
      ),
    },
  ];

  return (
    <div className={styles.tableContainer}>
      <h1>{title}</h1>
      <Divider />
      <Table
        scroll={{
          x: "200",
        }}
        dataSource={tableData ?? []}
        columns={columns}
        pagination={{ pageSize: 10, position: ["bottomCenter"] }}
      />
    </div>
  );
};

export default NyTable;
