import { Controller, useForm } from "react-hook-form";
import styles from "./style.module.css";
import { Button, Col, DatePicker, Flex, Input, Row, Select } from "antd";
import FormItem from "../FormItem/FormItem";
import Error from "../error-message";

interface IProps {
  submitData: (data: any) => void;
  loading?: boolean;
}
const NewsForm = ({ submitData, loading }: IProps) => {
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm();

  const onSubmit = (data: any) => {
    submitData(data);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={styles.formContainer}>
      <Row gutter={16}>
        <Col lg={8} md={12} sm={24} xs={24}>
          <FormItem label="Search for article" required>
            <Controller
              control={control}
              rules={{
                required: { value: true, message: "Search is required" },
              }}
              name="search"
              render={({ field }) => {
                return (
                  <Input.Search
                    size="middle"
                    {...field}
                    placeholder="Search news..."
                  />
                );
              }}
            />
            <Error errors={errors} name="search" />
          </FormItem>
        </Col>

        <Col lg={8} md={12} sm={24} xs={24}>
          <FormItem label="Select a date">
            <Controller
              control={control}
              name="date"
              render={({ field }) => {
                return (
                  <DatePicker
                    {...field}
                    size="middle"
                    style={{ width: "100%" }}
                    placeholder="Filter by date"
                  />
                );
              }}
            />
            <Error errors={errors} name="date" />
          </FormItem>
        </Col>

        <Col lg={8} md={12} sm={24} xs={24}>
          <FormItem label="Select a category">
            <Controller
              control={control}
              name="date"
              render={({ field }) => {
                return (
                  <Select
                    {...field}
                    size="middle"
                    defaultValue={"All"}
                    style={{ width: "100%" }}
                    placeholder="Category"
                  >
                    <Select.Option value="All">All</Select.Option>
                    {/* ... options for categories */}
                  </Select>
                );
              }}
            />
            <Error errors={errors} name="date" />
          </FormItem>
        </Col>

        <Col lg={8} md={12} sm={24} xs={24}>
          <FormItem label="News Sources" required>
            <Controller
              control={control}
              name="sources"
              rules={{
                required: {
                  value: true,
                  message: "Sources field is requried.",
                },
              }}
              render={({ field }) => {
                return (
                  <Select
                    {...field}
                    size="middle"
                    mode="multiple"
                    style={{ width: "100%" }}
                    placeholder="Sources"
                  >
                    <Select.Option value="news">News</Select.Option>
                    <Select.Option value="ny">Ny times</Select.Option>
                    <Select.Option value="guardian">The guardian</Select.Option>
                    {/* ... options for categories */}
                  </Select>
                );
              }}
            />
            <Error errors={errors} name="sources" />
          </FormItem>
        </Col>

        <Flex style={{ width: "100%" }} justify="center">
          <Button
            loading={loading}
            style={{ width: "30%" }}
            type="primary"
            htmlType="submit"
          >
            Submit
          </Button>
        </Flex>
      </Row>
    </form>
  );
};

export default NewsForm;
