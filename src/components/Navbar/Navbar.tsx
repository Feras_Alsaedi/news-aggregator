import { Flex } from "antd";
import styles from "./style.module.css";

const Navbar = () => {
  return (
    <nav className={styles.navbar}>
      <Flex justify="center" gap={0}>
        <h2 className="">News Aggregator</h2>
      </Flex>
    </nav>
  );
};

export default Navbar;
