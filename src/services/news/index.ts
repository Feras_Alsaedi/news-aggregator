import axios from "axios";

class NewsService {
  private apiKeyNewsAPI: string;
  private apiKeyGuardian: string;
  private apiKeyNYTimes: string;

  constructor() {
    this.apiKeyNewsAPI = import.meta.env.VITE_NEWAPI_KEY;
    this.apiKeyGuardian = import.meta.env.VITE_GUARDIAN_KEY;
    this.apiKeyNYTimes = import.meta.env.VITE_NY_KEY;
  }
  // Getting News API
  async fetchNewsAPI(
    searchTerm: string,
    category?: string,
    date?: string
  ): Promise<any[]> {
    const url = `${import.meta.env.VITE_NEWSAPI_URL}?q=${searchTerm}&apiKey=${
      this.apiKeyNewsAPI
    }${date ? `&from=${date}` : ""}`;
    const response = await axios.get<any>(url);
    return response.data.articles;
  }

  // Getting Guarfian news
  async fetchGuardian(searchTerm: string, section?: string): Promise<any[]> {
    const url = `${
      import.meta.env.VITE_GUARDIAN_URL
    }/search?q=${searchTerm}&api-key=${this.apiKeyGuardian}`;
    const response = await axios.get<any>(url);
    return response.data?.response?.results;
  }
  // Getting NY news
  async fetchNYTimes(
    searchTerm: string,
    newsDesk?: string,
    date?: string
  ): Promise<any[]> {
    const url = `${
      import.meta.env.VITE_NY_URL
    }/articlesearch.json?q=${searchTerm}&api-key=${this.apiKeyNYTimes}${
      date ? `&pub_date=${date}T00:00:00+0000` : ""
    }`;
    const response = await axios.get<any>(url);
    return response.data.response.docs;
  }
}

export default NewsService;
